package com.chen.controller;


import com.chen.common.Result;
import com.chen.pojo.User;
import com.chen.service.Impl.UserServiceImpl;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping("/sys")
@Api(value = "LoginController", tags = "账户操作")
public class LoginController {

    @Resource
    private UserServiceImpl userService;


    @PostMapping("/login")
    @ApiOperation("登录")
    public Result<?> login(@RequestBody User user) {

        return userService.login(user);
    }


    @PostMapping("/register")
    @ApiOperation("注册")
    public Result register(@RequestBody User user){
      return Result.success(userService.register(user));
    }


    @PutMapping("/forget")
    @ApiOperation("忘记密码")
    public Result forget(@RequestBody User user){
        return Result.success(userService.forget(user));
    }

    @PutMapping("/reset")
    @ApiOperation("重置密码")
    public Result reset(@RequestBody User user){
        return Result.success(userService.reset(user));
    }
}
