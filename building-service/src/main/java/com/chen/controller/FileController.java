package com.chen.controller;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSON;
import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.chen.common.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.util.List;


@RestController
@RequestMapping("/files")
@Api(value = "FileController", tags = "文件上传")
@SuppressWarnings("all")
public class FileController {

    @Value("${files.upload.path}")
    private String fileUploadPath;

    @Value("${server.ip}")
    private String serverIp;

    @Value("${server.port}")
    private String port;

    private static final String ip="http://localhost";


   /* @PostMapping("/upload")
    @ApiOperation(("文件上传"))
    public Result<?> upload(MultipartFile file) throws IOException {   //接受前台传过来的对象
        String flag = IdUtil.fastSimpleUUID();//定义文件唯一标识
        String originalFilename = file.getOriginalFilename();  //获取文件名称
        String rootFilePath = System.getProperty("user.dir") + "/src/main/resources/files/" + flag + originalFilename; //获取building-service文件夹路径+文件路径
        //将前台传过来的file对象写入到（磁盘）文件夹中
        FileUtil.writeBytes(file.getBytes(), rootFilePath);
        return Result.success(ip+":"+port+ "/files/" + flag);
    }*/

    @ApiOperation(value = "头像上传")
    @PostMapping("/upload")
    public  Result<?> upload(@RequestParam MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        String type = FileUtil.extName(originalFilename);
        long size = file.getSize();
        // 定义一个文件唯一的标识码
        String fileUUID = IdUtil.fastSimpleUUID() + StrUtil.DOT + type;
        File uploadFile = new File(fileUploadPath + fileUUID);
        // 判断配置的文件目录是否存在，若不存在则创建一个新的文件目录
        File parentFile = uploadFile.getParentFile();
        if(!parentFile.exists()) {
            parentFile.mkdirs();
        }
        String url;
        // 获取文件的md5
        // 上传文件到磁盘
        file.transferTo(uploadFile);
        // 数据库若不存在重复文件，则不删除刚才上传的文件
        url = "http://" + serverIp + ":"+port+"/files/" + fileUUID;

        return Result.success(url);
    }



    //富文本
    @PostMapping("/editor/load")
    public JSON editorUpload(MultipartFile file) throws IOException {  //接受前台的对象
       /* //定义文件唯一标识
        String flag = IdUtil.fastSimpleUUID();

        String originalFilename = file.getOriginalFilename();//获取文件名称
        String rootFilePath = System.getProperty("user.dir") + "/src/main/resources/files/" + flag + "_" + originalFilename;//获取文件路径
        FileUtil.writeBytes(file.getBytes(), rootFilePath); //使用hutool工具实现文件的写入
        String url = ip + ":" + port + "/files/" + flag;*/
        String originalFilename = file.getOriginalFilename();
        String type = FileUtil.extName(originalFilename);
        long size = file.getSize();
        // 定义一个文件唯一的标识码
        String fileUUID = IdUtil.fastSimpleUUID() + StrUtil.DOT + type;
        File uploadFile = new File(fileUploadPath + fileUUID);
        // 判断配置的文件目录是否存在，若不存在则创建一个新的文件目录
        File parentFile = uploadFile.getParentFile();
        if(!parentFile.exists()) {
            parentFile.mkdirs();
        }
        String url;
        // 获取文件的md5
        // 上传文件到磁盘
        file.transferTo(uploadFile);
        // 数据库若不存在重复文件，则不删除刚才上传的文件
        url = "http://" + serverIp + ":"+port+"/files/" + fileUUID;
        JSONObject json = new JSONObject();
        json.set("errno", 0);
        JSONArray arr = new JSONArray();
        JSONObject data = new JSONObject();
        arr.add(data);
        data.set("url", url);

        json.set("data", arr);
        return json;
    }



    @GetMapping("/{flag}")
    @ApiOperation("文件下载")
    public void getFiles(@PathVariable String flag, HttpServletResponse response) {
        OutputStream os;  // 新建一个输出流对象
        String basePath = System.getProperty("user.dir") + "/src/main/resources/files/";  // 定于文件上传的根路径
        List<String> fileNames = FileUtil.listFileNames(basePath);  // 获取所有的文件名称
        String fileName = fileNames.stream().filter(name -> name.contains(flag)).findAny().orElse("");  // 找到跟参数一致的文件
        try {
            if (StrUtil.isNotEmpty(fileName)) {
                response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
                response.setContentType("application/octet-stream");
                byte[] bytes = FileUtil.readBytes(basePath + fileName);  // 通过文件的路径读取文件字节流
                os = response.getOutputStream();   // 通过输出流返回文件
                os.write(bytes);
                os.flush();
                os.close();
            }
        } catch (Exception e) {
            System.out.println("文件下载失败");
        }
    }

}
