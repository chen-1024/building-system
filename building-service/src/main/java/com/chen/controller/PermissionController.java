package com.chen.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.mapper.BookMapper;

import com.chen.mapper.NewsMapper;
import com.chen.mapper.PermissionMapper;
import com.chen.pojo.Permission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/permission")
@Api(value = "PermissionController", tags = "菜单管理")
public class PermissionController   {


    //依赖注入
    @Resource
    PermissionMapper permissionMapper;


    //    新增
    @PostMapping
    @ApiOperation("新增")
    public Result<?> save(@RequestBody Permission Permission) {
        permissionMapper.insert(Permission);
        return Result.success();

    }

    //更新
    @PutMapping
    @ApiOperation("更新")
    public Result<?> update(@RequestBody Permission Permission) {
        permissionMapper.updateById(Permission);
        return Result.success();
    }

    @GetMapping("/all")
    @ApiOperation("查询")
    public Result<?> all() {
        return Result.success(permissionMapper.selectList(null));
    }

    @GetMapping("/{id}")
    @ApiOperation("查询")
    public Result<?> getById(@PathVariable Long id) {
        return Result.success(permissionMapper.selectById(id));
    }


    //删除
    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    public Result<?> delete(@PathVariable Long id) {
        permissionMapper.deleteById(id);
        return Result.success();
    }

    //    查询
    @ApiOperation("分页")
    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,  //页号
                              @RequestParam(defaultValue = "10") Integer pageSize, //一页展示的数量
                              @RequestParam(defaultValue = "") String search) {

//        模糊查询
        LambdaQueryWrapper<Permission> warapper = Wrappers.<Permission>lambdaQuery();
        if (StrUtil.isBlank(search)) {
            warapper.like(Permission::getName, search);
        }
        Page<Permission> userPage = permissionMapper.selectPage(new Page<>(pageNum, pageSize), warapper);
        return Result.success(userPage);

    }


}









