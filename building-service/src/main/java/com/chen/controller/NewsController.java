package com.chen.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.mapper.BookMapper;

import com.chen.mapper.NewsMapper;
import com.chen.pojo.News;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/news")
@Api(value = "NewsController", tags = "富文本编辑器")
public class NewsController {


    //依赖注入
    @Resource
    NewsMapper newsMapper;


    //     新增
    @PostMapping
    @ApiOperation("新增")
    public Result<?> save(@RequestBody News news) {
        news.setTime(new Date());
        newsMapper.insert(news);
        return Result.success();

    }

    //更新
    @PutMapping
    @ApiOperation("更新")
    public Result<?> update(@RequestBody News news) {
        newsMapper.updateById(news);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("查询")
    public Result<?> getById(@PathVariable Long id) {
        return Result.success(newsMapper.selectById(id));
    }

    //删除
    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    public Result<?> delete(@PathVariable Long id) {
        newsMapper.deleteById(id);
        return Result.success();
    }

    //    查询
    @ApiOperation("分页")
    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,  //页号
                              @RequestParam(defaultValue = "10") Integer pageSize, //一页展示的数量
                              @RequestParam(defaultValue = "") String search) {

//        模糊查询
        LambdaQueryWrapper<News> warapper = Wrappers.<News>lambdaQuery().like(News::getTitle, search);
        if (StrUtil.isBlank(search)) {
            warapper.like(News::getTitle, search);
        }
        Page<News> userPage = newsMapper.selectPage(new Page<>(pageNum, pageSize), warapper);
        return Result.success(userPage);

    }


}









