package com.chen.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.mapper.PermissionMapper;
import com.chen.mapper.RoleMapper;
import com.chen.pojo.Role;
import com.chen.pojo.RolePermisson;
import com.chen.pojo.User;
import com.chen.pojo.UserRole;
import com.chen.pojo.bean.UserInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/role")
@Api(value = "RoleController", tags = "角色管理")
public class RoleController extends BaseController {



    //依赖注入
    @Resource
    RoleMapper roleMapper;

    @Resource
    PermissionMapper permissionMapper;

    //    新增
    @PostMapping
    @ApiOperation("新增")
    public Result<?> save(@RequestBody Role Role) {
        roleMapper.insert(Role);
        return Result.success();

    }

    //更新
    @PutMapping
    @ApiOperation("更新")
    public Result<?> update(@RequestBody Role Role) {
        roleMapper.updateById(Role);
        return Result.success();
    }


    //更新
    @PutMapping("/changePermission")
    @ApiOperation("更新权限菜单")
    public Result<?> changePermission(@RequestBody Role role) {
        List<Integer> permissions = role.getPermissions();
        //先根据角色id册除所有的角色跟权限的绑定关系
        permissionMapper.deleteByRoleId(role.getId());
        //在新增新的绑定关系
        for (Integer permissionId : role.getPermissions()) {
            permissionMapper.insertRoleAndPermission(role.getId(),permissionId);
        }
        // 判断当前登录的用户角色是否包含了当前操作行的角色id，如果包含，则返回true，需要重新登录。
        //User user = getUser();
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        UserInfo user=new UserInfo();
        List<UserRole> userRoles = roleMapper.getByUserRoleUserId(user.getId());
        if (userRoles.stream().anyMatch(userRole -> userRole.getRoleId()==(role.getId()))) {
            return Result.success(true);
        }
       // roleMapper.updateById(Role);
        return Result.success(false);
    }

    @GetMapping("/{id}")
    @ApiOperation("查询")
    public Result<?> getById(@PathVariable Long id) {
        return Result.success(roleMapper.selectById(id));
    }

    @GetMapping("/all")
    @ApiOperation("查询所有")
    public Result<?> all() {
        return Result.success(roleMapper.selectList(null ));
    }


    //删除
    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    public Result<?> delete(@PathVariable Long id) {
        roleMapper.deleteById(id);
        return Result.success();
    }

    //    查询
    @ApiOperation("分页")
    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,  //页号
                              @RequestParam(defaultValue = "10") Integer pageSize, //一页展示的数量
                              @RequestParam(defaultValue = "") String search) {

//        模糊查询
        LambdaQueryWrapper<Role> warapper = Wrappers.<Role>lambdaQuery();
        if (StrUtil.isBlank(search)) {
            warapper.like(Role::getName, search);
        }
        //给角色设定绑定权限的数组
        Page<Role> RolePage = roleMapper.selectPage(new Page<>(pageNum, pageSize), warapper);
        List<Role> records = RolePage.getRecords();
        for (Role record : records) {
            int id = record.getId();
            List<Integer> permissions = permissionMapper.getRolePermissionByRoleId(id).stream().map(RolePermisson::getPermissionId).collect(Collectors.toList());
            record.setPermissions(permissions);
        }
        return Result.success(RolePage);

    }


}









