package com.chen.controller;


import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chen.common.Result;
import com.chen.pojo.Echarts;
import com.chen.mapper.EchartsMapper;
import com.chen.service.Impl.EchartsServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/area")
@Api(value = "EchartsController",tags = "数据分析")
public class EchartsController {

    @Resource
    private EchartsServiceImpl echartsService;

    @GetMapping
    @ApiOperation("查询")
    public Result<?> queryList(){
        List<Echarts> echarts = echartsService.list(Wrappers.lambdaQuery());
        return Result.success(echarts);
    }

}
