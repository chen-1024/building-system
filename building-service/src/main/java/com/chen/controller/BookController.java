package com.chen.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.mapper.BookMapper;
import com.chen.pojo.Book;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/book")
@Api(value = "BookController",tags = "图书管理")
@SuppressWarnings("all")
public class BookController {
    //依赖注入
    @Resource
    BookMapper bookMapper;


    //    新增
    @PostMapping
    @ApiOperation("新增")
    public Result<?> save(@RequestBody Book book) {

       bookMapper.insert(book);
        return Result.success();

    }

    //更新
    @PutMapping
    @ApiOperation("更新")
    public Result<?> update(@RequestBody Book book) {
        bookMapper.updateById(book);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("查询")
    public Result<?> getById(@PathVariable Long id){
        return Result.success(bookMapper.selectById(id));
    }



    @GetMapping("/{userId}")
    public Result<?> getByUserId(@PathVariable Integer userId) {
        return Result.success(bookMapper.findByUserId(userId));
    }

    //删除
    @DeleteMapping("/{id}")
    @ApiOperation("删除")
    public Result<?> delete(@PathVariable Long id) {
        bookMapper.deleteById(id);
        return Result.success();
    }

    //    查询
    @ApiOperation("分页")
    @GetMapping
    public Result<?> findPage(@RequestParam(defaultValue = "1") Integer pageNum,  //页号
                              @RequestParam(defaultValue = "10") Integer pageSize, //一页展示的数量
                              @RequestParam(defaultValue = "") String search) {

//        模糊查询
        LambdaQueryWrapper<Book> warapper = Wrappers.<Book>lambdaQuery().like(Book::getName, search);
        if (StrUtil.isBlank(search)) {
            warapper.like(Book::getName, search);
        }
        Page<Book> userPage =bookMapper.selectPage(new Page<>(pageNum, pageSize), warapper);
        return Result.success(userPage);

    }

    //批量删除
    @PostMapping("/deleteBatch")
    @ApiOperation("批量删除")
    public Result<?> deleteBatch(@RequestBody List<Integer> ids){
        bookMapper.deleteBatchIds(ids);
        return Result.success();
    }
}
