package com.chen.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.common.Result;
import com.chen.mapper.MessageMapper;
import com.chen.mapper.UserMapper;
import com.chen.pojo.Message;
import com.chen.pojo.User;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/message")
@Api(value = "MessageController",tags = "留言")
public class MessageController extends BaseController {
    @Resource
    private MessageMapper messageMapper;
    @Resource
    private UserMapper userMapper;
    @Resource
    HttpServletRequest request;

    @PostMapping
    @ApiOperation("添加留言")
    public Result<?> save(@RequestBody Message Message) {
        Message.setUsername(getUser().getUsername());
        Message.setTime(DateUtil.formatDateTime(new Date()));
        return Result.success(messageMapper.insert(Message));
    }

    @PutMapping
    @ApiOperation("更新留言")
    public Result<?> update(@RequestBody Message Message) {
        return Result.success(messageMapper.updateById(Message));
    }

    @DeleteMapping("/{id}")
    @ApiOperation("删除留言")
    public Result<?> delete(@PathVariable Long id) {
        messageMapper.deleteById(id);
        return Result.success();
    }

    @GetMapping("/{id}")
    @ApiOperation("查找留言")
    public Result<?> findById(@PathVariable Long id) {
        return Result.success(messageMapper.selectById(id));
    }

    @GetMapping
    @ApiOperation("查询所有留言")
    public Result<?> findAll() {
        return Result.success(messageMapper.selectList(null));
    }

    // 查询所有数据
    @GetMapping("/foreign/{id}")
    public Result<?> foreign(@PathVariable Integer id) {
        return Result.success(findByForeign(id));
    }

    @GetMapping("/page")
    @ApiOperation("分页")
    public Result<?> findPage(@RequestParam(required = false, defaultValue = "") String name,
                              @RequestParam(required = false, defaultValue = "1") Integer pageNum,
                              @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        LambdaQueryWrapper<Message> query = Wrappers.<Message>lambdaQuery().like(Message::getContent, name).orderByDesc(Message::getId);
        return Result.success(messageMapper.selectPage(new Page<>(pageNum, pageSize), query));
    }

    private List<Message> findByForeign(Integer foreignId) {
        // 根据 foreignId 0 查询所有的留言数据
        LambdaQueryWrapper<Message> queryWrapper = Wrappers.<Message>lambdaQuery().eq(Message::getForeignId, foreignId).orderByDesc(Message::getId);
        List<Message> list = messageMapper.selectList(queryWrapper);
        // 循环所有留言数据
        for (Message Message : list) {
            User user = new User();
            User one = userMapper.selectOne(Wrappers.<User>lambdaQuery().eq(User::getUsername, Message.getUsername()));

            if (StrUtil.isNotBlank(one.getAvatar())) {
                Message.setAvatar(one.getAvatar());
            } else {
                // 默认一个头像
                Message.setAvatar("https://img2.baidu.com/it/u=2906052251,1830965798&fm=26&fmt=auto");
            }
            Long parentId = Message.getParentId();
            // 判断当前的留言是否有父级，如果有，则返回父级   留言的信息
            // 原理：遍历所有留言数据，如果id跟当前留言信息的parentId相等，则将其设置为父级评论信息，也就是Message::setParentMessage
            list.stream().filter(c -> c.getId().equals(parentId)).findFirst().ifPresent(Message::setParentMessage);
        }
        return list;
    }

}
