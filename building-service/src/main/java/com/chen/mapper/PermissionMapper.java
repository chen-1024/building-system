package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.chen.pojo.Permission;
import com.chen.pojo.RolePermisson;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface PermissionMapper extends BaseMapper<Permission> {
    @Select("select *from role_permission where role_id=#{roleId}")
    List<RolePermisson> getRolePermissionByRoleId(int roleId);


    @Delete("delete from role_permission where role_id=#{roleId}")
    void deleteByRoleId(Integer roleId);


    @Insert("insert into role_permission(role_id, permission_id) values(#{roleId}, #{permissionId})")
    void insertRoleAndPermission(@Param("roleId") Integer roleId, @Param("permissionId") Integer permissionId);
}
