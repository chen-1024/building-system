package com.chen.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.pojo.Echarts;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface EchartsMapper extends BaseMapper<Echarts> {

}
