package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chen.pojo.Role;
import com.chen.pojo.UserRole;
import org.apache.ibatis.annotations.*;

import java.util.List;

@SuppressWarnings("all")
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    //查询所有的角色信息
    @Select("select *from user_role where user_id=#{userId}")
    List<UserRole> getByUserRoleUserId(int userId);

    @Insert("insert into user_role(user_id,role_id) value(#{userId},#{roleId})")
    int insertRole(@Param("userId") Integer userId, @Param("roleId")Integer roleId);

    @Select("select *from user_role where user_id=#{userId}")
    UserRole getUerRoleIsexist(Integer userId);


    @Select("select *from role where id=#{userId}")
    List<UserRole> getByUserId(int roleId);

    @Delete("delete from user_role where user_id = #{userId}")
    void deleteRoleByUserId(Integer userId);

    @Insert("insert into user_role(user_id, role_id) value(#{userId}, #{roleId})")
    void insertUserRole(@Param("userId") Integer userId, @Param("roleId")Integer roleId);
}
