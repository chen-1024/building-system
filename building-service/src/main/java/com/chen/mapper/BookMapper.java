package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.chen.pojo.Book;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


@Mapper
public interface BookMapper extends BaseMapper<Book> {
    // 根据用户id查询图书信息
    List<Book> findByUserId(@Param("userId") Integer userId);
}
