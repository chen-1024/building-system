package com.chen.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chen.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    //一对多查询
    Page<User> findPages(Page<User> page,@Param("username") String username);

    @Select("select * from user where qq_open_id = #{qqOpenId}")
    User getQqOpenId(@Param(value = "qqOpenId") String qqOpenId);
}
