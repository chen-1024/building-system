package com.chen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.common.Result;
import com.chen.pojo.User;



@SuppressWarnings("all")
public interface UserService extends IService<User> {

   //登录
    Result<?> login(User user);

    //注册
    Result<?> register(User user);

    //忘记密码
    Result<?> forget(User user);

    //重置密码
    Result<?> reset(User user);

}
