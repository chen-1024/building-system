package com.chen.service.Impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chen.mapper.EchartsMapper;
import com.chen.pojo.Echarts;
import com.chen.service.EchartsService;
import org.springframework.stereotype.Service;

@Service
public class EchartsServiceImpl extends ServiceImpl<EchartsMapper, Echarts> implements EchartsService {
}
