package com.chen.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chen.pojo.News;

public interface NewsService extends IService<News> {
}
