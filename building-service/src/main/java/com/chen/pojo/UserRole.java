package com.chen.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("user_role")
@Data
public class UserRole {

    @TableField("user_id")
    private Integer userId;

    @TableField("role_id")
    private Integer roleId;
}
