package com.chen.pojo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("echarts")
@Data
public class Echarts {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String area;
    private Integer count;
}
