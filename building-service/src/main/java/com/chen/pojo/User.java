package com.chen.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.List;
import java.util.Set;


@TableName("user")
@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({" hibernateLazyInitializer", " handler"})
@ApiModel(description = "用户实体")
public class User {
        @TableId(type = IdType.AUTO)  //设置自增
        private Integer id;
        @ApiModelProperty("用户名")
        private String username;
        private String password;
        private String nick;
        private Integer age;
        private String sex;
        private String address;
        private String avatar;
        private Integer role;
        private Integer status;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private String regtime;

    @TableField(exist = false)
    private String token;

    @TableField(exist = false)
    private List<Book> bookList;

    @TableField(exist = false)
    private Set<Permission> permissions;

    @TableField(exist = false)
    private List<Integer> roles;

    @TableField("qq_open_id")
    private String qqOpenId;


}
