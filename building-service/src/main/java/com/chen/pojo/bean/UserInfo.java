package com.chen.pojo.bean;

import com.chen.pojo.Permission;
import com.chen.pojo.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo {
    private int id;
    private String username;
    private String nick;
    private int age;
    private String sex;
    private String address;
    private String avatar;
    private int role;
    private int status;
    private String regtime;
    private String token;
    private Set<Permission> permissions;
    private String qqOpenId;


    public UserInfo(User user) {
        this.id = user.getId();
        this.age = user.getAge();
        this.sex = user.getSex();
        this.address = user.getAddress();
        this.role = user.getRole();
        this.username = user.getUsername();
        //this.avatar = user.getAvatar();
        this.nick = user.getNick();
        this.status = user.getStatus();
        this.regtime = user.getRegtime();
        this.token = user.getToken();
        this.permissions = user.getPermissions();
        this.avatar = user.getAvatar() != null ? user.getAvatar() : "https://img2.baidu.com/it/u=2285567582,1185119578&fm=26&fmt=auto";
        this.qqOpenId = user.getQqOpenId();
    }


}
