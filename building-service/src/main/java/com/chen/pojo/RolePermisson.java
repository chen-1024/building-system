package com.chen.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@TableName("role_permission")
@Data
public class RolePermisson {

    private Integer roleId;
    private Integer permissionId;
}
