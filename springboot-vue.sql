/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : springboot-vue

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 19/11/2021 16:13:44
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '出版时间',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES (2, '明朝那些事', 58.89, '张三', '2021-10-07 00:03:02', 12);
INSERT INTO `book` VALUES (3, '水浒传', 99.00, '施耐庵', '2021-10-06 00:00:02', 30);
INSERT INTO `book` VALUES (4, '西游记', 59.00, '吴承恩', '2021-10-05 00:00:02', 30);
INSERT INTO `book` VALUES (5, '东周列国志', 151.00, '无名', '2021-10-31 00:00:00', 24);
INSERT INTO `book` VALUES (6, '周公解梦', 54.00, '周公', '2021-10-27 00:00:00', 30);
INSERT INTO `book` VALUES (10, '白鹿原', 26.00, '陈忠实', '2021-10-26 01:01:02', 24);
INSERT INTO `book` VALUES (16, '史记', 45.00, '司马迁', '1561-03-09 03:02:03', 40);
INSERT INTO `book` VALUES (17, '张公子说书', 29.00, '公子张', '2021-10-14 08:02:12', 23);
INSERT INTO `book` VALUES (24, '且介亭文集', 88.80, '鲁迅', '2021-11-15 08:12:47', 65);

-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS `category`;
CREATE TABLE `category`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图书种类',
  `pid` int(11) NULL DEFAULT NULL COMMENT '父节点id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO `category` VALUES (1, '文学', NULL);
INSERT INTO `category` VALUES (2, '童书', 1);
INSERT INTO `category` VALUES (3, '社会科学', 1);
INSERT INTO `category` VALUES (4, '经济学', 1);
INSERT INTO `category` VALUES (5, '科普百科', 2);
INSERT INTO `category` VALUES (7, '法律', 3);

-- ----------------------------
-- Table structure for echarts
-- ----------------------------
DROP TABLE IF EXISTS `echarts`;
CREATE TABLE `echarts`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `area` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '地区',
  `count` int(11) NULL DEFAULT NULL COMMENT '人数',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of echarts
-- ----------------------------
INSERT INTO `echarts` VALUES (1, '安徽省', 5950);
INSERT INTO `echarts` VALUES (2, '湖南', 6570);
INSERT INTO `echarts` VALUES (3, '河北', 7185);
INSERT INTO `echarts` VALUES (4, '云南', 4596);
INSERT INTO `echarts` VALUES (5, '黑龙江', 3831);
INSERT INTO `echarts` VALUES (6, '重庆', 2884);
INSERT INTO `echarts` VALUES (7, '江苏', 7866);
INSERT INTO `echarts` VALUES (8, '辽宁', 4374);

-- ----------------------------
-- Table structure for images
-- ----------------------------
DROP TABLE IF EXISTS `images`;
CREATE TABLE `images`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `images` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '内容',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '评论人',
  `time` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '评论时间',
  `parent_id` bigint(20) NULL DEFAULT NULL COMMENT '父ID',
  `foreign_id` bigint(20) NULL DEFAULT 0 COMMENT '关联id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 45 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '留言表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (23, '哈哈哈', 'chen', '2021-05-24 17:13:45', 22, 0);
INSERT INTO `message` VALUES (24, '我们都爱吃大西瓜', 'chen', '2021-05-24 17:13:58', NULL, 0);
INSERT INTO `message` VALUES (27, 'hd', 'chenchao', '2021-11-05 17:19:40', 26, 0);
INSERT INTO `message` VALUES (28, '2222', 'fujie', '2021-11-08 14:24:08', NULL, 0);
INSERT INTO `message` VALUES (38, '大家好，我是路飞', 'admin', '2021-11-15 20:02:12', NULL, 0);
INSERT INTO `message` VALUES (39, '原来你就是海贼王啊', 'chenchao', '2021-11-15 20:03:06', 38, 0);
INSERT INTO `message` VALUES (40, '我是要成为海贼王的男人', 'fujie', '2021-11-15 21:26:38', 39, 0);
INSERT INTO `message` VALUES (41, '久仰久仰', 'chenchao', '2021-11-16 13:55:07', 40, 0);
INSERT INTO `message` VALUES (42, '喜欢你的每一天', 'bird', '2021-11-16 22:13:55', 27, 0);
INSERT INTO `message` VALUES (43, '你若安好，便是晴天', 'bird', '2021-11-16 22:14:38', NULL, 0);
INSERT INTO `message` VALUES (44, '👨‍🦱', 'chen', '2021-11-17 19:54:06', 41, 0);

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标题',
  `content` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '内容',
  `author` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '作者',
  `time` datetime(0) NULL DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES (5, '为什么要内卷?', '<p><b>累死自己，也要卷死别人</b></p><p><img src=\"http://localhost:80/files/bc3bbe1f7be345bdaa37e92d78e78890\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'chen', '2021-10-21 09:15:21');
INSERT INTO `news` VALUES (6, '累了也要看看风景', '<p><img src=\"https://img0.baidu.com/it/u=2771309493,4105971688&amp;fm=26&amp;fmt=auto\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'chen', '2021-10-21 21:19:51');
INSERT INTO `news` VALUES (11, '动漫', '<p><img src=\"http://localhost:9090/files/732d30cc8db041ae8ac2da025a517894\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', '吾是打工仔', '2021-10-22 01:03:45');
INSERT INTO `news` VALUES (12, '看看美女', '<p>这是谁家的小姐姐<br/><img src=\"http://localhost:9090/files/3d10fc5c5bbd43b5b40406be5abaed65\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', '吾是打工仔', '2021-10-22 01:10:37');
INSERT INTO `news` VALUES (14, '出路在哪里', '<p><img src=\"https://img1.baidu.com/it/u=3347714633,123231368&amp;fm=26&amp;fmt=auto\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', '远方', '2021-10-24 15:58:26');
INSERT INTO `news` VALUES (15, '如何日进斗金？', '<p>勤劳能致富，如果你坐享清福什么都不做，不靠双手去劳动，真是痴人做梦异想天开，更何是<em>日进斗</em>银恐怕对你今后的生话也难维持。<img src=\"http://localhost:80/files/8556e52341314a9b8e46520d138d0a2e\" contenteditable=\"false\" style=\"font-size: 14px; max-width: 100%;\"/></p><br/>', 'chen', '2021-10-24 19:47:17');
INSERT INTO `news` VALUES (16, '太空漫步，悠哉悠哉', '<p><img src=\"https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimg.yanlutong.com%2Fuploadimg%2Fimage%2F20210324%2F20210324143044_88216.gif&amp;refer=http%3A%2F%2Fimg.yanlutong.com&amp;app=2002&amp;size=f9999,10000&amp;q=a80&amp;n=0&amp;g=0n&amp;fmt=jpeg?sec=1637673212&amp;t=de3f9f3915f4e254fa32f51f114a45a6\" style=\"max-width:100%;\" contenteditable=\"false\"/></p>', 'chen', '2021-10-24 09:14:29');
INSERT INTO `news` VALUES (22, '我的富豪梦', '', '吾是打工仔', '2021-11-15 21:16:11');

-- ----------------------------
-- Table structure for permisson
-- ----------------------------
DROP TABLE IF EXISTS `permisson`;
CREATE TABLE `permisson`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '资源路径',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `icon` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图标',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permisson
-- ----------------------------
INSERT INTO `permisson` VALUES (1, 'Dashboard', '/dashboard', '首页', 'el-icon-location');
INSERT INTO `permisson` VALUES (2, 'User', '/users', '用户信息', 'el-icon-user-solid');
INSERT INTO `permisson` VALUES (3, 'AuditPage', '/AuditPage', '审核', 'el-icon-user');
INSERT INTO `permisson` VALUES (4, 'Permission', '/permission', '权限菜单', 'el-icon-menu');
INSERT INTO `permisson` VALUES (5, 'Role', '/role', '角色管理', 'el-icon-s-custom');
INSERT INTO `permisson` VALUES (6, 'UserRole', '/userRole', '用户角色', 'el-icon-user');
INSERT INTO `permisson` VALUES (7, 'Book', '/book', '阅读赏析', 'el-icon-reading');
INSERT INTO `permisson` VALUES (8, 'Order', '/order', '支付中心', 'el-icon-map-location');
INSERT INTO `permisson` VALUES (9, 'Base', '/base', '信息展示', 'el-icon-document');
INSERT INTO `permisson` VALUES (10, 'News', '/news', '我的动态', 'el-icon-s-promotion');
INSERT INTO `permisson` VALUES (11, 'Message', 'message', '评论与回复', 'el-icon-chat-line-round');
INSERT INTO `permisson` VALUES (12, 'Analysis', '/analysis', '数据分析', 'el-icon-s-data');
INSERT INTO `permisson` VALUES (13, 'Weather', '/weather', '天气情况', 'el-icon-cloudy-and-sunny');
INSERT INTO `permisson` VALUES (14, 'Map', '/map', '百度地图', 'el-icon-map-location');
INSERT INTO `permisson` VALUES (15, 'About', '/about', '请作者喝杯茶', 'el-icon-hot-water');
INSERT INTO `permisson` VALUES (24, 'Picture', '/picture', '照片墙', 'el-icon-picture ');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '名称',
  `comment` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, 'admin', '管理员');
INSERT INTO `role` VALUES (2, 'user', '普通用户');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `permission_id` int(11) NOT NULL COMMENT '资源id',
  PRIMARY KEY (`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (1, 1);
INSERT INTO `role_permission` VALUES (1, 2);
INSERT INTO `role_permission` VALUES (1, 3);
INSERT INTO `role_permission` VALUES (1, 4);
INSERT INTO `role_permission` VALUES (1, 5);
INSERT INTO `role_permission` VALUES (1, 6);
INSERT INTO `role_permission` VALUES (1, 7);
INSERT INTO `role_permission` VALUES (1, 8);
INSERT INTO `role_permission` VALUES (1, 9);
INSERT INTO `role_permission` VALUES (1, 10);
INSERT INTO `role_permission` VALUES (1, 11);
INSERT INTO `role_permission` VALUES (1, 12);
INSERT INTO `role_permission` VALUES (1, 13);
INSERT INTO `role_permission` VALUES (1, 14);
INSERT INTO `role_permission` VALUES (1, 15);
INSERT INTO `role_permission` VALUES (1, 24);
INSERT INTO `role_permission` VALUES (2, 1);
INSERT INTO `role_permission` VALUES (2, 7);
INSERT INTO `role_permission` VALUES (2, 8);
INSERT INTO `role_permission` VALUES (2, 10);
INSERT INTO `role_permission` VALUES (2, 11);
INSERT INTO `role_permission` VALUES (2, 12);
INSERT INTO `role_permission` VALUES (2, 13);
INSERT INTO `role_permission` VALUES (2, 14);
INSERT INTO `role_permission` VALUES (2, 15);
INSERT INTO `role_permission` VALUES (2, 24);

-- ----------------------------
-- Table structure for t_order
-- ----------------------------
DROP TABLE IF EXISTS `t_order`;
CREATE TABLE `t_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '名称',
  `total_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '总价',
  `pay_price` decimal(10, 2) NULL DEFAULT NULL COMMENT '实付款',
  `discount` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '优惠金额',
  `transport_price` decimal(10, 2) NULL DEFAULT 0.00 COMMENT '运费',
  `order_no` varchar(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '订单编号',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '用户账户',
  `create_time` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `pay_time` timestamp(0) NULL DEFAULT NULL COMMENT '支付时间',
  `state` int(1) NOT NULL DEFAULT 0 COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 21 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_order
-- ----------------------------
INSERT INTO `t_order` VALUES (21, '明朝那些事', 58.89, 58.89, 0.00, 0.00, 'd562880d-8759-4328-ac18-e5508ac2a068', 30, 'chenchao', '2021-11-18 21:19:07', NULL, 1);
INSERT INTO `t_order` VALUES (24, '水浒传', 99.00, 99.00, 0.00, 0.00, 'dc759384-abd2-474c-bcbf-998300a798c0', 30, 'chenchao', '2021-11-18 21:12:34', NULL, 0);
INSERT INTO `t_order` VALUES (49, '且介亭文集', 88.80, 88.80, 0.00, 0.00, '9990b255-9aaf-4c25-8026-8398e7b242df', 30, 'chenchao', '2021-11-19 11:53:27', NULL, 1);
INSERT INTO `t_order` VALUES (52, '东周列国志', 151.00, 151.00, 0.00, 0.00, 'b64f2be5-a319-4ae8-b81d-5fa8b7d1e019', 30, 'chenchao', '2021-11-19 11:59:04', NULL, 1);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `username` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `nick` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '昵称',
  `age` int(11) NULL DEFAULT NULL COMMENT '年龄',
  `sex` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '性别',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '住址',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头像',
  `role` int(255) NULL DEFAULT NULL COMMENT '1：管理员 2：普通用户',
  `status` int(255) NULL DEFAULT NULL COMMENT '1:通过审核 2：未审核',
  `regtime` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '注册时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 80 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (12, '张超', '123456', '小神仙', 20, '男', '安徽省亳州市', 'http://localhost:9090/files/24aa0ec141e74d2e937faec1b9aea089', 0, 0, '2021-11-13 13:53:14');
INSERT INTO `user` VALUES (23, '陈俊磊', '123456', '小迷', 0, '男', '安徽省黄山市祁门县\n', 'http://localhost:9090/files/2604a4ea39874de0ba04b57a194d9095', 0, 0, '2021-11-13 13:46:11');
INSERT INTO `user` VALUES (30, 'chenchao', '123456', 'chen', 21, '男', '安徽省芜湖市湾沚区', 'http://localhost:9090/files/a7a6611e9c934bcb8dddcc51d48b2de8', 1, 1, '2021-11-13 13:46:41');
INSERT INTO `user` VALUES (40, 'chen', '123456', '远方', 21, '男', '安徽省芜湖市湾沚区', 'http://localhost:9090/files/d9fc54419f4a47579274b5f0c2afd08d', 1, 1, '2021-11-13 13:46:16');
INSERT INTO `user` VALUES (43, 'admin', '123456', 'admin', 20, '男', '安徽省芜湖市', 'http://localhost:9090/files/1ca75a1c68d44925b5ba89017fb586bc', 1, 1, '2021-11-13 13:46:22');
INSERT INTO `user` VALUES (57, 'bird', '123456', '凉予', 0, '女', '安徽省芜湖市弋江区', 'http://localhost:9090/files/4fea17666b81458e90b6d9c89efff118', 0, 1, '2021-11-13 13:46:19');
INSERT INTO `user` VALUES (64, 'zhangsan', '123456', 'zhang', 0, '男', '安徽省六安市', 'http://localhost:9090/files/31b223fac67546eeb6248233c43c2427', 0, 1, '2021-11-13T15:03:47.000Z');
INSERT INTO `user` VALUES (65, 'fujie', '123456', '吾是打工仔', 20, '男', '安徽省舒城县城关镇', 'http://localhost:9090/files/279c02f314a84421a9f48a0018637c83', 0, 1, '2021-11-14T03:51:57.000Z');
INSERT INTO `user` VALUES (66, 'shaying', '123456', 'SHAYING', 20, '男', '安徽省舒城县五里村', 'http://localhost:9090/files/566618e6c2f443b1b127d5bb294e3582', 2, 1, '2021-11-14T03:14:57.000Z');
INSERT INTO `user` VALUES (79, 'zhangchao', '123456', NULL, 0, NULL, NULL, 'http://localhost:9090/files/12e2caa5e03949119efd496c22103edd', 0, 0, '2021-11-15 14:47:51');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (12, 2);
INSERT INTO `user_role` VALUES (23, 2);
INSERT INTO `user_role` VALUES (30, 1);
INSERT INTO `user_role` VALUES (40, 2);
INSERT INTO `user_role` VALUES (43, 2);
INSERT INTO `user_role` VALUES (57, 2);
INSERT INTO `user_role` VALUES (64, 2);
INSERT INTO `user_role` VALUES (65, 2);
INSERT INTO `user_role` VALUES (66, 2);
INSERT INTO `user_role` VALUES (79, 2);

SET FOREIGN_KEY_CHECKS = 1;
