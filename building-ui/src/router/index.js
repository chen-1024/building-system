import Vue from 'vue'
import VueRouter from 'vue-router'
import Layout from '../layout/Layout.vue'
import AuthRedirect from "../views/auth-redirect/auth-redirect";

Vue.use(VueRouter)

// 解决报错
const originalPush = VueRouter.prototype.push
const originalReplace = VueRouter.prototype.replace
// push
VueRouter.prototype.push = function push (location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
    return originalPush.call(this, location).catch(err => err)
}
// replace
VueRouter.prototype.replace = function push (location, onResolve, onReject) {
    if (onResolve || onReject) return originalReplace.call(this, location, onResolve, onReject)
    return originalReplace.call(this, location).catch(err => err)
}

const routes = [
    {
        path: '/',
        name: 'Layout',
        component: Layout,
        redirect:'/store',
        children: [
            {
                path: "/authRedirect",
                name: 'auth-redirect',
                component: AuthRedirect,

            },
            //首页
            {
                path: 'dashboard',
                name: 'Dashboard',
                component: () =>import('../views/Dashboard'),
            },

            {
                path: '/person',
                name: 'Person',
                component: () => import("../views/Person")
            },
           /* //用户
            {
                path: 'user',
                name: 'user',
                component: () =>import('../views/User'),
            },
            //管理员审核
            {
                path: 'AuditPage',
                name: 'AuditPage',
                component: () =>import('../views/AuditPage'),
            },
            //数据分析
            {
                path: '/analysis',
                name: 'analysis',
                component: () => import("../views/Analysis")
            },
            //个人资料
            {
                path: '/person',
                name: 'Person',
                component: () => import("../views/Person")
            },
            //图书
            {
                path: '/book',
                name: 'Book',
                component: () => import("../views/Book")
            },
            //信息展示
            {
                path: '/base',
                name: 'Base',
                component: () => import("../views/Base")
            },
            //留言
            {
                path: '/message',
                name: 'Message',
                component: () => import("../views/Message")
            },
            //天气
            {
                path: '/weather',
                name: 'Weather',
                component: () => import("../views/Weather")
            },
            //动态
            {
                path: '/news',
                name: 'News',
                component: () => import("../views/News")
            },
            //商城
            {
                path: '/store',
                name: 'Store',
                component: () => import("../views/Store")
            },
            //地图
            {
                path: '/map',
                name: 'Map',
                component: () => import("../views/Map")
            },
            //关于作者
            {
                path: '/about',
                name: 'About',
                component: () => import("../views/About")
            },*/


        ]
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import("../views/Login")
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import("../views/Register")
    },
    //商城
    {
        path: '/store',
        name: 'Store',
        component: () =>import('../views/Store'),
    },
]

const router = new VueRouter({
    routes
})


//动态路由，防止刷新产生空白页面
activeRouter()

function activeRouter() {
    const userStr = sessionStorage.getItem("user")
    if (userStr) {
        const user = JSON.parse(userStr)
        let root = {
            path: '/',
            name: 'Layout',
            component: Layout,
            redirect: "/dashboard",
            children: []
        }
        user.permissions.forEach(p => {
            let obj = {
                path: p.path,
                name: p.name,
                component: () => import("@/views/" + p.name)
            };
            root.children.push(obj)
        })
        if (router) {
            router.addRoute(root)
        }
    }
}


export default router
