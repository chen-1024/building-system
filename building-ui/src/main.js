import Vue from 'vue'
import App from './App.vue'
import router from './router'

//引入global.css\
import './assets/css/global.css'

//引入element
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

//国际化
import locale from 'element-ui/lib/locale/lang/zh-CN'

//引入echarts
import * as echarts from 'echarts'

Vue.prototype.$echarts = echarts


//使用百度地图
import BaiduMap from 'vue-baidu-map'

//使用
Vue.use(ElementUI,{ locale ,size:"small"});

Vue.config.productionTip = false

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')

Vue.use(BaiduMap, {
    // ak 是在百度地图开发者平台申请的密钥 详见 http://lbsyun.baidu.com/apiconsole/key */
    ak: 'jG10dFdq37FymeLH8wW32P5eoD14nXOA'
})
